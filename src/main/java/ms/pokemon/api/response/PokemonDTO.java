package ms.pokemon.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PokemonDTO {

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private String id;

}
