package ms.pokemon.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PokemonMoreDataDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("baseHappiness")
    private Long baseHappiness;

    @JsonProperty("captureRate")
    private Long captureRate;

    @JsonProperty("sprite")
    private String sprite;

    @JsonProperty("description")
    private String description;

    @JsonProperty("type")
    private Types type;

    @JsonProperty("evolutions")
    private List<Evolution> evolutions;

    @Data
    public static class Types {

        @JsonProperty("type1")
        private String type1;

        @JsonProperty("type2")
        private String type2;
    }

    @Data
    public static class Evolution {

        @JsonProperty("name")
        private Long number;

        @JsonProperty("name")
        private String name;

        @JsonProperty("sprite")
        private String sprite;
    }
}
