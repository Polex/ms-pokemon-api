package ms.pokemon.api.restclient;

import ms.pokemon.api.config.FeignConfig;
import ms.pokemon.api.entity.PokeApiResponse;
import ms.pokemon.api.entity.PokemonFormEntity;
import ms.pokemon.api.entity.PokemonMoreInfoEntity;
import ms.pokemon.api.entity.PokemonSpeciesEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "pokemonClient", url = "${feign.pokemon-api-service}", configuration = {FeignConfig.class})
public interface PokemonApiClient {

    @GetMapping(value = "/api/v2/pokemon-species")
    PokeApiResponse<List<PokemonSpeciesEntity>> getAllPokemonSpecies(@RequestParam(value = "offset", defaultValue = "0") Long offset);

    @GetMapping("/api/v2/pokemon-species/{filter}")
    PokemonMoreInfoEntity getAllPokemonSpeciesByIdOrName(@PathVariable("filter") String filter);

    @GetMapping("/api/v2/pokemon-form/{id}")
    PokemonFormEntity getPokemonFormById(@PathVariable("id") Long id);

}
