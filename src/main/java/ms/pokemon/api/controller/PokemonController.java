package ms.pokemon.api.controller;

import ms.pokemon.api.response.ApiResponse;
import ms.pokemon.api.response.PokemonDTO;
import ms.pokemon.api.response.PokemonMoreDataDTO;
import ms.pokemon.api.service.PokemonApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/pokemon")
public class PokemonController {

    @Autowired
    private PokemonApiService pokemonApiService;

    @GetMapping
    public ResponseEntity<ApiResponse<List<PokemonDTO>>> getPokemon(@RequestParam(value = "offset", required = false) Long offset){
        return ResponseEntity.ok(this.pokemonApiService.getPokemon(offset));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PokemonMoreDataDTO> getPokemonById(@PathVariable(value = "id") Long id){
        return ResponseEntity.ok(this.pokemonApiService.getPokemonById(id));
    }

}

