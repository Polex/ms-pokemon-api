package ms.pokemon.api.mapper;

import ms.pokemon.api.entity.PokemonFormEntity;
import ms.pokemon.api.entity.PokemonMoreInfoEntity;
import ms.pokemon.api.entity.PokemonSpeciesEntity;
import ms.pokemon.api.response.PokemonDTO;
import ms.pokemon.api.response.PokemonMoreDataDTO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PokemonApiMapper {

    public PokemonDTO parsePokemonSpeciesToDto(PokemonSpeciesEntity pokemonSpeciesEntity) {
        PokemonDTO pokemonDTO = new PokemonDTO();
        pokemonDTO.setName(pokemonSpeciesEntity.getName().toUpperCase());
        pokemonDTO.setId(pokemonSpeciesEntity.getUrl().replace("https://pokeapi.co/api/v2/pokemon-species/", "").replace("/", ""));
        return pokemonDTO;
    }

    public List<PokemonDTO> parsePokemonSpeciesToDtoList(List<PokemonSpeciesEntity> pokemonSpeciesEntities) {
        if(CollectionUtils.isEmpty(pokemonSpeciesEntities)){
            return Collections.emptyList();
        }
        return pokemonSpeciesEntities.stream()
                .map(this::parsePokemonSpeciesToDto)
                .collect(Collectors.toList());

    }

    public PokemonMoreDataDTO parsePokemonMoreInfoEntityToDto(PokemonMoreInfoEntity pokemonMoreInfoEntity, PokemonFormEntity pokemonFormEntity) {
        PokemonMoreDataDTO pokemonMoreDataDTO = new PokemonMoreDataDTO();
        pokemonMoreDataDTO.setName(pokemonMoreInfoEntity.getName());
        pokemonMoreDataDTO.setBaseHappiness(pokemonMoreInfoEntity.getBaseHappiness());
        pokemonMoreDataDTO.setCaptureRate(pokemonMoreInfoEntity.getCaptureRate());
        if(Objects.nonNull(pokemonFormEntity.getSprites())) {
            pokemonMoreDataDTO.setSprite(pokemonFormEntity.getSprites().getFrontDefault());
        }

        pokemonMoreDataDTO.setDescription(getDescriptionDto(pokemonMoreInfoEntity.getFlavorTextEntries()));
        pokemonMoreDataDTO.setType(parseTypesEntityToDto(pokemonFormEntity.getTypes()));

        return pokemonMoreDataDTO;
    }

    private String getDescriptionDto(List<PokemonMoreInfoEntity.FlavorTextEntry> flavorTextEntries){
        String description = Strings.EMPTY;
        if(!CollectionUtils.isEmpty(flavorTextEntries)){
            Optional<PokemonMoreInfoEntity.FlavorTextEntry> flavorTextEntry = flavorTextEntries.stream()
                    .filter(flavorText -> "es".equals(flavorText.getLanguage().getName()) && "sword".equals(flavorText.getVersion().getName()))
                    .findFirst();
            if(flavorTextEntry.isPresent()){
                description = flavorTextEntry.get().getFlavorText();
            }
        }
        return description;
    }

    private PokemonMoreDataDTO.Types parseTypesEntityToDto(List<PokemonFormEntity.TypeInfo> typeInfos){
        PokemonMoreDataDTO.Types types = new PokemonMoreDataDTO.Types();
        if(!CollectionUtils.isEmpty(typeInfos)){
            types.setType1(typeInfos.get(0).getType().getName());
            if(typeInfos.size() == 2){
                types.setType2(typeInfos.get(1).getType().getName());
            }
        }
        return types;
    }

}
