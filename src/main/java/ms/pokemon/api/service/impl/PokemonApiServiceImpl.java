package ms.pokemon.api.service.impl;

import ms.pokemon.api.entity.PokeApiResponse;
import ms.pokemon.api.entity.PokemonFormEntity;
import ms.pokemon.api.entity.PokemonMoreInfoEntity;
import ms.pokemon.api.entity.PokemonSpeciesEntity;
import ms.pokemon.api.mapper.PokemonApiMapper;
import ms.pokemon.api.response.ApiResponse;
import ms.pokemon.api.response.PokemonDTO;
import ms.pokemon.api.response.PokemonMoreDataDTO;
import ms.pokemon.api.restclient.PokemonApiClient;
import ms.pokemon.api.service.PokemonApiService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PokemonApiServiceImpl implements PokemonApiService {

    @Autowired
    private PokemonApiClient pokemonApiClient;

    @Autowired
    private PokemonApiMapper pokemonApiMapper;


    @Override
    public ApiResponse<List<PokemonDTO>> getPokemon(Long offset) {
        PokeApiResponse<List<PokemonSpeciesEntity>> listPokeApiResponse = pokemonApiClient.getAllPokemonSpecies(offset);
        List<PokemonDTO> pokemonDTOList = pokemonApiMapper.parsePokemonSpeciesToDtoList(listPokeApiResponse.getResults());

        return ApiResponse.<List<PokemonDTO>>builder()
                .data(pokemonDTOList)
                .count(listPokeApiResponse.getCount())
                .next(buildNextOrPrevious(listPokeApiResponse.getNext()))
                .previous(buildNextOrPrevious(listPokeApiResponse.getPrevious()))
                .build();
    }

    @Override
    public PokemonMoreDataDTO getPokemonById(Long id) {
        PokemonMoreInfoEntity pokemonMoreInfoEntity = pokemonApiClient.getAllPokemonSpeciesByIdOrName(id.intValue() + "");
        PokemonFormEntity pokemonFormEntity = pokemonApiClient.getPokemonFormById(id);
        PokemonMoreDataDTO pokemonMoreDataDTO = pokemonApiMapper.parsePokemonMoreInfoEntityToDto(pokemonMoreInfoEntity, pokemonFormEntity);
        return pokemonMoreDataDTO;
    }



    private String buildNextOrPrevious(String link) {
        if(Strings.isEmpty(link)){
            return null;
        }
        return link.replace("https://pokeapi.co/api/v2/pokemon-species", "/pokemon-api/pokemon")
                .replace("&limit=20", Strings.EMPTY);
    }
}
