package ms.pokemon.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PokemonMoreInfoEntity {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("base_happiness")
    private Long baseHappiness;

    @JsonProperty("capture_rate")
    private Long captureRate;

    @JsonProperty("evolution_chain")
    private EvolutionChain evolutionChain;

    @JsonProperty("flavor_text_entries")
    private List<FlavorTextEntry> flavorTextEntries;


    @Data
    public static class EvolutionChain {

        @JsonProperty("url")
        private String url;
    }


    @Data
    public static class FlavorTextEntry {

        @JsonProperty("flavor_text")
        private String flavorText;

        @JsonProperty("language")
        private Language language;

        @JsonProperty("version")
        private Version version;

    }


    @Data
    public static class Language {

        @JsonProperty("name")
        private String name;

    }

    @Data
    public static class Version {

        @JsonProperty("name")
        private String name;
    }

}
