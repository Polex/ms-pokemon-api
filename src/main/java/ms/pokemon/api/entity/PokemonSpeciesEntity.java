package ms.pokemon.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PokemonSpeciesEntity {


    @JsonProperty("name")
    private String name;

    @JsonProperty("url")
    private String url;

}
