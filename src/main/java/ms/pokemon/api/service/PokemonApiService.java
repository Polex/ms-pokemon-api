package ms.pokemon.api.service;

import ms.pokemon.api.response.ApiResponse;
import ms.pokemon.api.response.PokemonDTO;
import ms.pokemon.api.response.PokemonMoreDataDTO;

import java.util.List;

public interface PokemonApiService {

    ApiResponse<List<PokemonDTO>> getPokemon(Long offset);

    PokemonMoreDataDTO getPokemonById(Long id);

}
