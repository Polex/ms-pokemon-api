package ms.pokemon.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PokemonFormEntity {

    @JsonProperty("sprites")
    private Sprites sprites;

    @JsonProperty("types")
    private List<TypeInfo> types;

    @Data
    public static class Sprites {

        @JsonProperty("front_default")
        private String frontDefault;

    }


    @Data
    public static class TypeInfo {

        @JsonProperty("slot")
        private Long slot;

        @JsonProperty("type")
        private Type type;
    }

    @Data
    public static class Type {

        @JsonProperty("name")
        private String name;
    }

}
