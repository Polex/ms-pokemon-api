package ms.pokemon.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MsPokemonApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsPokemonApiApplication.class, args);
	}



}
